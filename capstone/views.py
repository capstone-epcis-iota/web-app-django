from django.shortcuts import render
from .models import Product, Event
from django.http import HttpResponse
import dateutil.parser
import requests
import json


# Create your views here.
def index(request):
  return render(request, 'capstone/index.html')

def epcs(request):
  epc_list = {}
  url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/epcs'
  response = requests.get(url)
  epc_list = response.json()['epcsList']
  return render(request, 'capstone/epcs.html', {'epc_list': epc_list})

def search_id_sgtin(request):
  links = {}
  flag = 0
  body = {}
  list = {}
  site = "urn:epc:id:sgtin:"
  if 'code' in request.GET:
    code = request.GET['code']
    url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/epcs/urn:epc:id:sgtin:%s' % code
    response = requests.get(url)
    if "not" in response.text:
      flag = 1
    else:
      body = response.json()['EPCIS']
      links = response.json()['_links']
      list = response.json()['EPCIS']['epcisBody']['eventList']
  return render(request, 'capstone/search.html', {'body': body, 'list': list, 'site': site, 'flag': flag, 'links': links})

def search_id_grai(request):
  links = {}
  flag = 0
  body = {}
  list = {}
  site = "urn:epc:id:grai:"
  if 'code' in request.GET:
    code = request.GET['code']
    url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/epcs/urn:epc:id:grai:%s' % code
    response = requests.get(url)
    if "not" in response.text:
      flag = 1
    else:
      body = response.json()['EPCIS']
      links = response.json()['_links']
      list = response.json()['EPCIS']['epcisBody']['eventList']
  return render(request, 'capstone/search.html', {'body': body, 'list': list, 'site': site, 'flag': flag, 'links': links})


def search_id_giai(request):
  links = {}
  flag = 0
  body = {}
  list = {}
  site = "urn:epc:id:giai:"
  if 'code' in request.GET:
    code = request.GET['code']
    url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/epcs/urn:epc:id:giai:%s' % code
    response = requests.get(url)
    if "not" in response.text:
      flag = 1
    else:
      body = response.json()['EPCIS']
      links = response.json()['_links']
      list = response.json()['EPCIS']['epcisBody']['eventList']
  return render(request, 'capstone/search.html', {'body': body, 'list': list, 'site': site, 'flag': flag, 'links': links})

def search_id_sscc(request):
  links = {}
  flag = 0
  body = {}
  list = {}
  site = "urn:epc:id:sscc:"
  if 'code' in request.GET:
    code = request.GET['code']
    url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/epcs/urn:epc:id:sscc:%s' % code
    response = requests.get(url)
    if "not" in response.text:
      flag = 1
    else:
      body = response.json()['EPCIS']
      links = response.json()['_links']
      list = response.json()['EPCIS']['epcisBody']['eventList']
  return render(request, 'capstone/search.html', {'body': body, 'list': list, 'site': site, 'flag': flag, 'links': links})

def about(request):
  return render(request, 'capstone/about.html')

def bizsteps(request):
  list = {}
  url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/bizSteps'
  response = requests.get(url)
  list = response.json()['bizSteps']
  return render(request, 'capstone/bizsteps.html', {'list': list})

def step(request):
  links = {}
  flag = 0
  body = {}
  list = {}
  step_name = {}
  if 'step' in request.GET:
    step = request.GET['step']
    url = 'http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/bizSteps/%s' % step
    response = requests.get(url)
    if "not" in response.text:
      flag = 1
    else:
      body = response.json()['EPCIS']
      links = response.json()['_links']
      list = response.json()['EPCIS']['epcisBody']['eventList']
      step_name = response.json()['EPCIS']['epcisBody']['eventList'][0]['bizStep']
  return render(request, 'capstone/step.html', {'body': body, 'list': list, 'flag': flag, 'links': links, 'step_name': step_name})