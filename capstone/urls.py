from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('epcs', views.epcs, name ='epcs'),
    path('search_id/sgtin/', views.search_id_sgtin, name='search-id-sgtin'),
    path('search_id/grai/', views.search_id_grai, name='search-id-grai'),
    path('search_id/giai/', views.search_id_giai, name='search-id-giai'),
    path('search_id/sscc/', views.search_id_sscc, name='search-id-sscc'),
    path('about', views.about, name="about"),
    path('bizsteps', views.bizsteps, name="bizsteps"),
    path('bizsteps/', views.step, name="step"),
]
