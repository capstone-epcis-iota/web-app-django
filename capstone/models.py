from django.db import models

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Event(models.Model):
    name = models.CharField('Event Type', max_length=120)
    event_date = models.DateTimeField('Event Date')
    event_loc = models.CharField(max_length=120)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


