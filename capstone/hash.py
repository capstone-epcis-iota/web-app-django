import hashlib
import requests

#Use this function for large files, local testing
def md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def get_md5(url):
    try:
        response = requests.get(url)
    except requests.ConnectionError:
        print("Connection Error")
    hash_md5 = hashlib.md5()
    hash_md5.update(response.content)
    return hash_md5.hexdigest()

def main():
    file_name = "test_doc.pdf"
    url = "https://dagrs.berkeley.edu/sites/default/files/2020-01/sample.pdf"
    print(get_md5(url))
    #print(md5(file_name))

if __name__ == "__main__":
    main()