from django import template
import urllib.parse
import hashlib
import requests

register = template.Library()

def getCode(code):
  if "sgtin" in code:
    x = code.split("urn:epc:id:sgtin:")[1]
  if "grai" in code:
    x = code.split("urn:epc:id:grai:")[1]
  if "giai" in code:
    x = code.split("urn:epc:id:giai:")[1]
  if "sscc" in code:
    x = code.split("urn:epc:id:sscc:")[1]
  return "?code="+x

def getEncode(code):
  if "sgtin" in code:
    x = code.split("urn:epc:id:sgtin:")[1]
  if "grai" in code:
    x = code.split("urn:epc:id:grai:")[1]
  if "giai" in code:
    x = code.split("urn:epc:id:giai:")[1]
  if "sscc" in code:
    x = code.split("urn:epc:id:sscc:")[1]
  y = urllib.parse.quote(x, safe='')
  return "?code="+y

def stepEncode(link):
  x = link.split("http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/bizSteps/")[1]
  y = urllib.parse.quote(x, safe='') 
  return "?step="+y

def pre(str):
  return "/?step="+str

def remove_pre(str):
  if "urn:epcglobal:cbv:bizstep:" in str:
    x = str.split("urn:epcglobal:cbv:bizstep:")[1]
    return x.upper()
  else:
    return str.upper()

def verifyMd5(url, existing_md5):
    response = requests.get(url)
    hash_md5 = hashlib.md5()
    hash_md5.update(response.content)
    if hash_md5.hexdigest() == existing_md5:
      #existing_md5 = "PDF Passed Verification"
      #return existing_md5
      return "PDF Passed Verification"
    else:
      #existing_md5 = "PDF Failed Verification"
      #return existing_md5
      return "PDF Failed Verification"
  

register.filter('getCode', getCode)
register.filter('getEncode', getEncode)
register.filter('stepEncode', stepEncode)
register.filter('verifyMd5', verifyMd5)
register.filter('pre', pre)
register.filter('remove_pre', remove_pre)
